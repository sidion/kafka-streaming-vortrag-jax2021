package de.sidion.kafka.streamingclient;

import de.sidion.kafka.streamingclient.domain.NewCustomerEvent;
import de.sidion.kafka.streamingclient.domain.NewsletterEvent;
import de.sidion.kafka.streamingclient.domain.OrderEvent;
import de.sidion.kafka.streamingclient.serde.SerdeService;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.*;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.time.Duration;
import java.util.Properties;
import java.util.UUID;

@SpringBootApplication
@Slf4j
public class StreamingClientJoinApplication implements CommandLineRunner {


    public static void main(final String[] args) {
        SpringApplication.run(StreamingClientJoinApplication.class, args);
    }

    @Override
    public void run(final String... args) throws Exception {
        final SerdeService serdeService = new SerdeService();


        // building stream configuration
        final Properties streamsConfiguration = new Properties();
        streamsConfiguration.put(StreamsConfig.APPLICATION_ID_CONFIG, UUID.randomUUID().toString());
        streamsConfiguration.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        streamsConfiguration.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        streamsConfiguration.put("commit.interval.ms", 100);


        final StreamsBuilder builder = new StreamsBuilder();

        // create the value joiner
        final ValueJoiner<OrderEvent, NewCustomerEvent, NewsletterEvent> valueJoiner = new ValueJoiner<>() {
            @Override
            public NewsletterEvent apply(final OrderEvent orderEvent, final NewCustomerEvent newCustomerEvent) {
                if (newCustomerEvent.getConsentNL()) {
                    return NewsletterEvent.builder()
                            .userId(newCustomerEvent.getUserId())
                            .email(newCustomerEvent.getEmail())
                            .category(orderEvent.getCategory())
                            .build();
                }
                return null;
            }
        };

        // read from topic new-customers into a stream and rekey it to userid
        final KStream<String, NewCustomerEvent> newCustomerEventStream =
                builder.stream("new-customers", Consumed.with(Serdes.String(), serdeService.getSerdeForNewCustomerEvent()))
                        .selectKey((key, val) -> val.getUserId());

        // read from topic product-orders into a stream and rekey it to userid
        final KStream<String, OrderEvent> orderEventStream =
                builder.stream("product-orders", Consumed.with(Serdes.String(), serdeService.getSerdeForOrderDto()))
                        .selectKey((key, val) -> val.getUserId());

        // create a join window
        final JoinWindows twoHoursWindow = JoinWindows.of(Duration.ofMinutes(120));

        // join streams
        final KStream<String, NewsletterEvent> joinedStream =
                orderEventStream.join(newCustomerEventStream, valueJoiner, twoHoursWindow,
                        StreamJoined.with(Serdes.String(), serdeService.getSerdeForOrderDto(),
                                serdeService.getSerdeForNewCustomerEvent()));
        joinedStream.foreach((key, val) -> log.info("key: " + key + ", val: " + val));

        // start streaming
        System.out.println("start streaming ...");
        final KafkaStreams kafkaStreams = new KafkaStreams(builder.build(), streamsConfiguration);
        kafkaStreams.start();
    }
}
