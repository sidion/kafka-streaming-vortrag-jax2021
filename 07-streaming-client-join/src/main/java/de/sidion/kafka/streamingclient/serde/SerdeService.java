package de.sidion.kafka.streamingclient.serde;

import de.sidion.kafka.streamingclient.domain.NewCustomerEvent;
import de.sidion.kafka.streamingclient.domain.OrderEvent;
import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.serialization.Serializer;

import java.util.HashMap;
import java.util.Map;

public class SerdeService {

    public Serde<OrderEvent> getSerdeForOrderDto() {
        final Map<String, Object> serdeProps = new HashMap<>();

        final Serializer<OrderEvent> serializer = new JsonPOJOSerializer<>();
        serdeProps.put("JsonPOJOClass", OrderEvent.class);
        serializer.configure(serdeProps, false);

        final Deserializer<OrderEvent> deserializer = new JsonPOJODeserializer<>();
        serdeProps.put("JsonPOJOClass", OrderEvent.class);
        deserializer.configure(serdeProps, false);

        final Serde<OrderEvent> serde = Serdes.serdeFrom(serializer, deserializer);

        return serde;
    }

    public Serde<NewCustomerEvent> getSerdeForNewCustomerEvent() {
        final Map<String, Object> serdeProps = new HashMap<>();

        final Serializer<NewCustomerEvent> serializer = new JsonPOJOSerializer<>();
        serdeProps.put("JsonPOJOClass", NewCustomerEvent.class);
        serializer.configure(serdeProps, false);

        final Deserializer<NewCustomerEvent> deserializer = new JsonPOJODeserializer<>();
        serdeProps.put("JsonPOJOClass", NewCustomerEvent.class);
        deserializer.configure(serdeProps, false);

        final Serde<NewCustomerEvent> serde = Serdes.serdeFrom(serializer, deserializer);

        return serde;
    }


}
