package de.sidion.kafka.streamingclient.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
public class NewCustomerEvent {

    private String userId;
    private String email;
    private String name;
    private Boolean consentNL;

}
