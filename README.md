# Streaming mit Apache Kafka

Code-Beispiele aus dem Vortrag

## Kafka Installation

Als Voraussetzung wird ein laufender Apache Kafka benötigt. Dazu gibt es im Ordner kafka ein docker-compose Script welches einen einfachen Kafka inkl. Zookeeper hochfährt.

```bash
docker-compose -f kafka/docker-compose.yml up -d
```

## Topics

Die für die Beispiele benötigten Topics können mit folgenden Befehlen angelegt werden:

```bash
kafka-topics.sh --create --topic product-orders --partitions 3 --replication-factor 1 --zookeeper localhost:2181
kafka-topics.sh --create --topic new-customers --partitions 3 --replication-factor 1 --zookeeper localhost:2181
```


## Daten

Im Unterordner kafka/data sind unterschiedliche Beispieldaten im JSON-Format abgelegt welche dann in den Kafka eingespielt werden können.

```bash
kafka-console-producer.sh --broker-list localhost:9092 --topic product-orders < orders.txt
kafka-console-producer.sh --broker-list localhost:9092 --topic new-customers < new-customers.txt
```

