package de.sidion.kafka.streamingclient.serde;

import de.sidion.kafka.streamingclient.domain.AccumulatedProductEvent;
import de.sidion.kafka.streamingclient.domain.OrderEvent;
import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.serialization.Serializer;

import java.util.HashMap;
import java.util.Map;

public class SerdeService {

    public Serde<OrderEvent> getSerdeForOrderDto() {
        final Map<String, Object> serdeProps = new HashMap<>();

        final Serializer<OrderEvent> serializer = new JsonPOJOSerializer<>();
        serdeProps.put("JsonPOJOClass", OrderEvent.class);
        serializer.configure(serdeProps, false);

        final Deserializer<OrderEvent> deserializer = new JsonPOJODeserializer<>();
        serdeProps.put("JsonPOJOClass", OrderEvent.class);
        deserializer.configure(serdeProps, false);

        final Serde<OrderEvent> serde = Serdes.serdeFrom(serializer, deserializer);

        return serde;
    }

    public Serde<AccumulatedProductEvent> getSerdeForAccumulatedProductDto() {
        final Map<String, Object> serdeProps = new HashMap<>();

        final Serializer<AccumulatedProductEvent> serializer = new JsonPOJOSerializer<>();
        serdeProps.put("JsonPOJOClass", AccumulatedProductEvent.class);
        serializer.configure(serdeProps, false);

        final Deserializer<AccumulatedProductEvent> deserializer = new JsonPOJODeserializer<>();
        serdeProps.put("JsonPOJOClass", AccumulatedProductEvent.class);
        deserializer.configure(serdeProps, false);

        final Serde<AccumulatedProductEvent> serde = Serdes.serdeFrom(serializer, deserializer);

        return serde;
    }


}
