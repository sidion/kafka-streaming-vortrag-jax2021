package de.sidion.kafka.streamingclient;

import de.sidion.kafka.streamingclient.domain.AccumulatedProductEvent;
import de.sidion.kafka.streamingclient.domain.OrderEvent;
import org.apache.kafka.streams.kstream.ValueTransformer;
import org.apache.kafka.streams.processor.ProcessorContext;
import org.apache.kafka.streams.state.KeyValueStore;

public class OrderEventValueTransformer implements ValueTransformer<OrderEvent, AccumulatedProductEvent> {

    private KeyValueStore<String, AccumulatedProductEvent> stateStore;
    private final String storeName;
    private ProcessorContext context;

    public OrderEventValueTransformer(String storeName) {
        this.storeName = storeName;
    }


    @Override
    public void init(ProcessorContext context) {
        this.context = context;
        stateStore = (KeyValueStore) this.context.getStateStore(storeName);
    }

    @Override
    public AccumulatedProductEvent transform(OrderEvent orderEvent) {

        AccumulatedProductEvent accumulatedProductEvent = stateStore.get(orderEvent.getProductId());
        if(accumulatedProductEvent == null){
            accumulatedProductEvent = AccumulatedProductEvent.builder()
                    .productId(orderEvent.getProductId())
                    .productName(orderEvent.getProductName())
                    .totalCount(0)
                    .build();
        }
        accumulatedProductEvent.setTotalCount(accumulatedProductEvent.getTotalCount()+orderEvent.getCount());

        stateStore.put(accumulatedProductEvent.getProductId(), accumulatedProductEvent);

        return accumulatedProductEvent;
    }

    @Override
    public void close() {

    }
}
