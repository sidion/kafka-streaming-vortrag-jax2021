package de.sidion.kafka.streamingclient.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
public class OrderEvent {

    private String orderId;
    private String userId;
    private String productId;
    private String productName;
    private String category;
    private Integer unitPrice;
    private Integer count;

}
