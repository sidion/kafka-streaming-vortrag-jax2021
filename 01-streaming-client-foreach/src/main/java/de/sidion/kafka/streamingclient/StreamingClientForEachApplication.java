package de.sidion.kafka.streamingclient;

import de.sidion.kafka.streamingclient.domain.OrderEvent;
import de.sidion.kafka.streamingclient.serde.SerdeService;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.KStream;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Properties;
import java.util.UUID;

@SpringBootApplication
@Slf4j
public class StreamingClientForEachApplication implements CommandLineRunner {


    public static void main(String[] args) {
        SpringApplication.run(StreamingClientForEachApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        final SerdeService serdeService = new SerdeService();


        // building stream configuration
        Properties streamsConfiguration = new Properties();
        streamsConfiguration.put(StreamsConfig.APPLICATION_ID_CONFIG, UUID.randomUUID().toString());
        streamsConfiguration.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        streamsConfiguration.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        streamsConfiguration.put("commit.interval.ms", 100);


        final StreamsBuilder builder = new StreamsBuilder();

        // read from topic product-orders into a stream
        final KStream<String, OrderEvent> orderDtoStream =
                builder.stream("product-orders", Consumed.with(Serdes.String(), serdeService.getSerdeForOrderDto()));


        // use foreach to log single entries
        orderDtoStream.foreach((key, val) -> System.out.println("key: " + key + ", val: " + val));


        // start streaming
        System.out.println("start streaming ...");
        final KafkaStreams kafkaStreams = new KafkaStreams(builder.build(), streamsConfiguration);
        kafkaStreams.start();
    }
}
