package de.sidion.kafka.streamingclient;

import de.sidion.kafka.streamingclient.domain.CountedProduct;
import de.sidion.kafka.streamingclient.domain.OrderEvent;
import de.sidion.kafka.streamingclient.serde.SerdeService;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.*;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Properties;
import java.util.UUID;

@SpringBootApplication
@Slf4j
public class StreamingClientGroupByAndAggregationApplication implements CommandLineRunner {


    public static void main(final String[] args) {
        SpringApplication.run(StreamingClientGroupByAndAggregationApplication.class, args);
    }

    @Override
    public void run(final String... args) throws Exception {
        final SerdeService serdeService = new SerdeService();


        // building stream configuration
        final Properties streamsConfiguration = new Properties();
        streamsConfiguration.put(StreamsConfig.APPLICATION_ID_CONFIG, UUID.randomUUID().toString());
        streamsConfiguration.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        streamsConfiguration.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        streamsConfiguration.put("commit.interval.ms", 100);


        final StreamsBuilder builder = new StreamsBuilder();

        // read from topic product-orders into a stream and rekey it to productId
        final KStream<String, OrderEvent> orderEventStream =
                builder.stream("product-orders", Consumed.with(Serdes.String(), serdeService.getSerdeForOrderEvent()))
                        .selectKey((key, val) -> val.getProductId());

        // aggregate OrderEvents to CountedProduct
        final KTable<String, CountedProduct> aggregate = orderEventStream.groupByKey(
                Grouped.with(Serdes.String(), serdeService.getSerdeForOrderEvent()))
                .aggregate(() -> new CountedProduct(0),
                        (key, val, agg) -> {
                            agg.setCount(agg.getCount() + 1);
                            return agg;
                        },
                        Materialized.with(Serdes.String(), serdeService.getSerdeForCountedProduct()));

        aggregate.toStream().foreach((key, val) -> log.info("key: " + key + ", val: " + val));

        // start streaming
        System.out.println("start streaming ...");
        final KafkaStreams kafkaStreams = new KafkaStreams(builder.build(), streamsConfiguration);
        kafkaStreams.start();
    }
}
