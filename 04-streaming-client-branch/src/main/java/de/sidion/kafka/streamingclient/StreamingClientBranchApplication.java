package de.sidion.kafka.streamingclient;

import de.sidion.kafka.streamingclient.domain.OrderEvent;
import de.sidion.kafka.streamingclient.serde.SerdeService;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.KStream;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Properties;
import java.util.UUID;

@SpringBootApplication
@Slf4j
public class StreamingClientBranchApplication implements CommandLineRunner {


    public static void main(String[] args) {
        SpringApplication.run(StreamingClientBranchApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        final SerdeService serdeService = new SerdeService();


        // building stream configuration
        Properties streamsConfiguration = new Properties();
        streamsConfiguration.put(StreamsConfig.APPLICATION_ID_CONFIG, UUID.randomUUID().toString());
        streamsConfiguration.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        streamsConfiguration.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        streamsConfiguration.put("commit.interval.ms", 100);


        final StreamsBuilder builder = new StreamsBuilder();

        // read from topic product-orders into a stream
        final KStream<String, OrderEvent> orderEventStream =
                builder.stream("product-orders", Consumed.with(Serdes.String(), serdeService.getSerdeForOrderEvent()));

        // branch stream to 3 streams. the predicate is the category name
        final KStream<String, OrderEvent>[] branchedOrderStreams = orderEventStream.branch(
                (key, val) -> val.getCategory().equals("PC"),
                (key, val) -> val.getCategory().equals("Fotografie"),
                (key, val) -> val.getCategory().equals("Smartphones"));
        branchedOrderStreams[0].foreach((key, val) -> log.info("Branch PC: {}", val.toString()));
        branchedOrderStreams[1].foreach((key, val) -> log.info("Branch Fotografie: {}", val.toString()));
        branchedOrderStreams[2].foreach((key, val) -> log.info("Branch Smartphones: {}", val.toString()));


        // start streaming
        System.out.println("start streaming ...");
        final KafkaStreams kafkaStreams = new KafkaStreams(builder.build(), streamsConfiguration);
        kafkaStreams.start();
    }
}
